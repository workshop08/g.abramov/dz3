package MyPackage;

public class Armor extends Item{
    private int defense;

    public Armor(ItemTypes type, int status, int defense){
        super(type, status);
        this.defense = defense;
    }

    public void printInfo() {
        super.printInfo();
        System.out.println("Defense - " + this.defense + ".\n");
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int a) {
        this.defense = a;
    }

}
