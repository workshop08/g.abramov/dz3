package MyPackage;

import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;


public class BattleGraphics extends Application{

    private Unit unit1, unit2;

    public BattleGraphics(Unit unit1, Unit unit2) {
        this.unit1 = unit1;
        this.unit2 = unit2;
    }

    public void human(int x1, int y1, ObservableList rootChildren, Paint color) {

        Circle head = new Circle();
        head.setCenterX(x1);
        head.setCenterY(y1);
        head.setRadius(30);
        head.setFill(color);
        rootChildren.add(head);

        Line body = new Line(x1, y1 + 30, x1, y1 + 150);
        rootChildren.add(body);

        Line leg1 = new Line(x1, y1 + 150, x1 - 25, y1 + 270);
        rootChildren.add(leg1);

        Line leg2 = new Line(x1, y1 + 150, x1 + 25, y1 + 270);
        rootChildren.add(leg2);

        Line arm1 = new Line(x1, y1 + 60, x1 + 40, y1 + 140);
        rootChildren.add(arm1);

        Line arm2 = new Line(x1, y1 + 60, x1 - 40, y1 + 140);
        rootChildren.add(arm2);


    }

    @Override
    public void start(Stage primaryStage) {

        Group root = new Group();
        Scene scene = new Scene(root, 1000, 700);
        primaryStage.setTitle("Le epic");
        primaryStage.setScene(scene);

        ObservableList rootChildren = root.getChildren();

        Text name1 = new Text(100, 50, unit1.getName());
        name1.setFont(Font.font("century gothic", 20));
        name1.setFill(Color.BLACK);
        rootChildren.add(name1);

        human(150, 100, rootChildren, Color.BLACK);

        Text hp1 = new Text(100, 430, "HP - " + unit1.getArmor());
        hp1.setFont(Font.font("century gothic", 20));
        hp1.setFill(Color.BLACK);
        unit1.addPropertyChangeListener(unit1.HP, pcEvent -> updateHp(hp1, unit1.armor));
        rootChildren.add(hp1);

        Text name2 = new Text(810, 50, unit2.getName());
        name2.setFont(Font.font("century gothic", 20));
        name2.setFill(Color.BLACK);
        rootChildren.add(name2);

        human(850, 100, rootChildren, Color.BLACK);

        Text hp2 = new Text(810, 430, "HP - " + unit2.getArmor());
        hp2.setFont(Font.font("century gothic", 20));
        hp2.setFill(Color.BLACK);
        unit2.addPropertyChangeListener(unit2.HP, pcEvent -> updateHp(hp2, unit2.armor));
        rootChildren.add(hp2);

        unit1.battleWith(unit2);

        primaryStage.show();

    }

    private void updateHp(Text hp1, int armor) {
        hp1.setText("HP - " + armor);
    }

}
