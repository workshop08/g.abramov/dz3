package MyPackage;

import java.util.Random;

public class BattleThread extends Thread {

    Unit unit1, unit2;

    public BattleThread(Unit unit1, Unit unit2) {
        this.unit1 = unit1;
        this.unit2 = unit2;
    }

    public void run() {
        Random rnd = new Random();
        int damage;
        boolean isActive = true;
        synchronized (this.unit1) {
            while (isActive) {
                if ((this.unit2.armor <= 0) || (this.unit1.armor <= 0)) {
                    isActive = false;
                    this.interrupt();
                } else {
                    damage = this.unit1.damage - 2 + rnd.nextInt(5);
                    if (damage < 0) {
                        damage = 0;
                    }
//                    System.out.println(this.unit1.name + " attacks.");
//                    System.out.println("Damage - " + damage);
                    int oldValue = this.unit2.armor;
                    this.unit2.armor -= damage;
                    int newValue = this.unit2.armor;
                    this.unit2.pcSupport.firePropertyChange(this.unit2.HP, oldValue, newValue);
                    try {
                        this.unit1.wait((11 - this.unit1.moves) * 500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
