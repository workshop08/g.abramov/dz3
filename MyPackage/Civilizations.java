package MyPackage;

public enum Civilizations {

    ENGLAND ("England"),
    POLYNESIA ("Polynesia"),
    RUSSIA ("Russia"),
    HUNS ("Huns"),
    NETHERLANDS ("Netherlands"),
    DENMARK ("Denmark"),
    CHINA ("China");

    private String title;

    Civilizations(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

}