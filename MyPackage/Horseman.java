package MyPackage;

public class Horseman extends Unit {
    private int const_moves;
    private String mode;

    public Horseman(Civilizations civ, int armor, int damage, int moves, String name) {
        super(civ, armor, damage, moves, name);
        this.type = "horseman";
        this.const_moves = moves;
        this.mode = "Normal";
    }

    public void printInfo() {
        super.printInfo();
        System.out.println("Mode - " + this.mode + ".\n");
    }

    public void changeMode() {
        if (this.mode == "Normal") {
            this.damage += const_moves - 1;
            this.moves = 1;
            this.mode = "Offensive";
        }
        else {
            this.moves = const_moves;
            this.armor -= const_moves + 1;
            this.mode = "Normal";
        }
    }

    public String getMode() {
        return this.mode;
    }

}