package MyPackage;

public abstract class Item {
    protected int status, id;
    protected ItemTypes type;
    protected static int i = 0;

    protected Item(ItemTypes type, int status) {
        this.type = type;
        this.status = status;
        this.id = i;
        i++;
    }

    public void printInfo() {
        System.out.println("ID - " + this.id + ".");
        System.out.println("This is " + this.type.getTitle() + ".");
        System.out.println("Status - " + this.status + ".");
    }

    public int getId() {
        return this.id;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int a) {
        this.status = a;
    }

    public String getType() {
        return this.type.getTitle();
    }

    public void setType(ItemTypes a) {
        this.type = a;
    }
}