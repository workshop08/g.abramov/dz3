package MyPackage;

public enum ItemTypes {

    SPEAR ("Spear"),
    SWORD ("Sword"),
    SHIELD ("Shield"),
    HELMET ("Helmet"),
    BOW ("Bow"),
    NONE("None");

    private String title;

    ItemTypes(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

}
