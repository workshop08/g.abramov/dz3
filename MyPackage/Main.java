package MyPackage;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static final String LogFileName = "log.txt";

    public void start(Stage primaryStage) {
        Archer p_a = new Archer(Civilizations.POLYNESIA, 100, 2, 10, "ToothPaste");
        Scout n_s = new Scout(Civilizations.NETHERLANDS, 100, 20, 2, "G.E.C.K.");
        BattleGraphics view = new BattleGraphics(p_a, n_s);
        view.start(primaryStage);
    }

    public static void main(String[] args) {

//        Logger logger = Logger.getLogger(Main.class.getName());
//        logger.setLevel(Level.INFO);
//        try {
//            FileHandler fileHandler = new FileHandler(LogFileName);
//            logger.addHandler(fileHandler);
//            SimpleFormatter simpleFormatter = new SimpleFormatter();
//            fileHandler.setFormatter(simpleFormatter);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        Warrior r_w1 = new Warrior(Civilizations.RUSSIA, 10, 1, 2, "kook");
//        Warrior r_w2 = new Warrior(Civilizations.RUSSIA, 6, 2, 2, "chook");
//        Horseman h_h = new Horseman(Civilizations.HUNS, 3, 5, 3, "Torch");
//        Weapon w1 = new Weapon(ItemTypes.SPEAR, 100, 3);
//        Weapon w2 = new Weapon(ItemTypes.BOW, 100, 3);
//        Armor a1 = new Armor(ItemTypes.HELMET, 100, 4);
//        p_a.equipArmor(a1);
//        r_w1.battleWith(n_s);

        launch();

    }

}
