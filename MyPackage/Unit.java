package MyPackage;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;

public abstract class Unit {
    public static final String HP = "hp";
    protected SwingPropertyChangeSupport pcSupport = new SwingPropertyChangeSupport(this);
    protected int armor, damage, moves, id;
    protected String type, name;
    protected Civilizations civ;
    protected ItemTypes item;
    protected static int i = 0;

    protected Unit(Civilizations civ, int armor, int damage, int moves, String name) {
        this.armor = armor;
        this.damage = damage;
        this.moves = moves;
        this.civ = civ;
        this.id = i;
        this.name = name;
        this.item = ItemTypes.NONE;
        i++;
    }

    public void printInfo() {
        System.out.println("ID - " + this.id + ".");
        System.out.println("Name - " + this.name + ".");
        System.out.println("This is " + this.civ.getTitle() + "'s " + this.type + ".");
        System.out.println("Armor - " + this.armor + ".");
        System.out.println("Damage - " + this.damage + ".");
        if (this.item != ItemTypes.NONE) {
            System.out.println("Item - " + this.item.getTitle() + ".");
        }
        System.out.println("Moves - " + this.moves + ".");
    }

    public void battleWith(Unit opp) {
//        boolean isDone = false;
        BattleThread bf1 = new BattleThread(this, opp);
        BattleThread bf2 = new BattleThread(opp, this);
        bf1.start();
        bf2.start();
//        while (!isDone) {
//            if ((bf1.isInterrupted()) && (bf2.isInterrupted())) {
//                isDone = true;
//                if ((this.armor <= 0) && (opp.armor <= 0)) {
//                    System.out.println("Draw.\n");
//                } else if (opp.armor <= 0) {
//                    System.out.println(this.name + " has won (ID - " + this.id + ").\n");
//                } else {
//                    System.out.println(opp.name + " has won (ID - " + opp.id + ").\n");
//                }
//            }
//        }
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String a) {
        this.name = a;
    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int a) {
        this.damage = a;
    }

    public int getMoves() {
        return this.moves;
    }

    public void setMoves(int a) {
        this.moves = a;
    }

    public String getCiv() {
        return this.civ.getTitle();
    }

    public void setCiv(Civilizations a) {
        this.civ = a;
    }

    public int getArmor() {
        return this.armor;
    }

    public void setArmor(int a) {
        this.armor = a;
    }

    public String getItem() {
        return this.item.getTitle();
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcSupport.addPropertyChangeListener(name, listener);
    }


}