package MyPackage;

public class Warrior extends Unit {

    public Warrior(Civilizations civ, int armor, int damage, int moves, String name) {
        super(civ, armor, damage, moves, name);
        this.type = "warrior";
    }

    public void printInfo() {
        super.printInfo();
        System.out.println();
    }

    public void equipWeapon(Weapon item) {
        if (((item.getType() == "Spear") || (item.getType() == "Sword")) && (this.item == ItemTypes.NONE)) {
            this.item = item.type;
            this.damage += item.getDamage();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

    public void equipArmor(Armor item) {

        if ((item.getType() == "Shield") && (this.item == ItemTypes.NONE))  {
            this.item = item.type;
            this.armor += item.getDefense();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

}
