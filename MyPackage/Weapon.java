package MyPackage;

public class Weapon extends Item{
    private int damage;

    public Weapon(ItemTypes type, int status, int damage){
        super(type, status);
        this.damage = damage;
    }

    public void printInfo() {
        super.printInfo();
        System.out.println("Damage - " + this.damage + ".\n");
    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int a) {
        this.damage = a;
    }

}